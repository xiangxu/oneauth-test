###Prepare:
1. docker and docker-compose 1.8+
2. download the code source
3. python3.5(only need for testing)
###Install:
    auto/install
###Test:
    auto/test
    auto/sa
###COVERAGE
    auto/coverage
###Uninstall
    auto/uninstall

Note: You must provide the environment variables in `env-confg/dev.env`